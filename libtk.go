// Copyright 2024 The libtk8_6-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libtk8_6 is deprecated.
//
package libtk8_6 // import "modernc.org/libtk8_6"
